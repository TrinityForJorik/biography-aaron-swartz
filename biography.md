
# Aaron Swartz: Internet Hacktivist

Aaron Swartz was an American programmer, entrepreneur, and hacktivist, he is best
known for his work related to `reddit`. His work spans across most of the web
technologies that are in use today, just to name a few are markdown, RSS and the
most impressive in my opinion the Creative Commons license, in fact, this essay
is written in markdown.

Aaron Swartz started working on these projects at an early age, often termed as
a child prodigy. There is a very common theme across all of his works, being the
open access to information. One of the first projects that he made was a website
that would let people add information on any topic they wanted, similar to
Wikipedia he called `The Info`. This was at an age of twelve. This project
starts the advent of his life revolving around open information.

His early work on the `RSS` system sprang him into the Silicon Valley, at an age
of thirteen he was helping shape the technology used by millions. `RSS` has made
sharing news and blogs infinitely easier, he was on the committee that drafted
the first version of `RSS`. At this point in time in his life he was deeply
involved with the web, and with internet growing, it faced one of its first
challenges. The copyright laws at the time were not made with the internet in
mind, he collaborated with Lawrence Lessig, the founder of the `Creative Commons`
license.

He was clearly an overachiever for his age, so he was offered a scholarship program
at Stanford University, but he did not feel like he was growing, he often wrote
about his experience at college and expressed his displeasure with college, and at
the same time he was working for an upcoming startup `Infogami`, the
the startup was under the tech-incubator `Y-Combinator`, founded by `Paul Graham`.
He immediately took the opportunity and dropped out of college and moved to a
different city, but the company didn't do well and they decided to merge with
another `Y-Combinator` startup called `reddit`. Reddit is one of the biggest
website today, and it has its own culture and is very centric to internet a
website.

After reddit Aaron moved on to a string of new projects, the reason for his was
mostly because he rejected the business world he did share the same vision
as reddit after its sale. These new projects were all around open access, to
name a few of them, `watchdogs.net`, `openlibrary.org`. Then came around `PACER`
which stands for Public Access to Court Electronic Records, the federal government
had an archaic system for people to access the records of the courts and Aaron
decided that the citizens needed to have easy access to these documents; together
with `Carl Malamud` of `public.resource.org` he made a website where they hosted
documents downloaded from PACER. This is time he caught the laws eyes, but he didn't
stop here, his passion for open-access made him investigate the academic journal
system. He set up a machine in the MIT libraries to download academic journals from
the JSTOR database to an external storage machine, he was soon cut off from the
MIT network but he found a server rack in the basement and connected his laptop.
This event started all of Aaron's legal troubles, the MIT staff set up a
surveillance camera in the basement, at this point he was being prosecuted with
federal charges by the Department of Justice.

Aaron still wasn't phased by all this legal action, his biggest contribution as a
hacktivist was yet to happen. In October of 2011, there was a bill introduced
known as the SOPA, which stood for Stop Online Piracy Act; the bill aimed to
expand the ability of the law enforcement to combat online copyright infringement,
but like most regulations on the internet and especially on a complicated subject
like copyright the law was overly stringent. Aaron along with the people from
`demandprogress.org` were able to turn the votes in the Congress from a majority
for the bill to a landslide majority against the bill.

Aaron Swartz contributed his life to internet hacktivism, he could have been
one of the many successful tech-entrepreneurs but he chose a more noble path. All
of his works from a young age, have been to provide open access for all. Hopefully
as people from the same community we can keep his legacy alive and continue
his work.
